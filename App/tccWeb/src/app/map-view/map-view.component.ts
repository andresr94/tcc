import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-map-view',
  templateUrl: './map-view.component.html',
  styleUrls: ['./map-view.component.css']
})
export class MapViewComponent implements OnInit {
  
  private azul: boolean = true;
  private verde: boolean = true;
  private vermelho: boolean = true;
  private amarelo: boolean = true;

  constructor() { }

  ngOnInit() {
  }

  showTodos(): void {
    this.azul = true;
    this.verde = true;
    this.vermelho = true;
    this.amarelo = true;
  }

  toggleAzul(): void {
    this.azul = !this.azul;
  }

  toggleVerde(): void {
    this.verde = !this.verde;
  }
  toggleVermelho(): void {
    this.vermelho = !this.vermelho;
  }
  toggleAmarelo(): void {
    this.amarelo = !this.amarelo;
  }

}
